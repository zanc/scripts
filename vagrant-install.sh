#!/bin/sh
# vim: se ts=2 sw=2 et:

#vendor=laravel
#box=homestead
#version=6.3.0
#provider=virtualbox

if [ $# -lt 3 ]; then
  echo "usage: ${0##*/} box version provider" >&2
  exit 1
fi

vendor=${1%/*}
box=${1#*/}
shift
version=$1; shift
provider=$1; shift

boxname=$vendor-VAGRANTSLASH-$box-$version-$provider.box
vagrantboxdir="$HOME/.vagrant.d/boxes"

if [ ! -f "$boxname" ]; then
  echo "==> $boxname doesn't exist" >&2
  echo "==> Have you tried 'vagrant-fetch.sh $vendor/$box $version $provider\` ?" >&2
  exit 1
fi
if [ -d "$vagrantboxdir/$vendor-VAGRANTSLASH-$box" ]; then
  echo "==> $vagrantboxdir/$vendor-VAGRANTSLASH-$box is already exists"
  exit 0
fi
vagrant box add $vendor/$box $boxname
retval="$?"
if [ "$retval" != "0" ]; then
  echo "==> vagrant returned non-zero" >&2
  exit 1
fi
mv "$vagrantboxdir/$vendor-VAGRANTSLASH-$box/0" "$vagrantboxdir/$vendor-VAGRANTSLASH-$box/$version"
printf '%s' "https://app.vagrantup.com/$vendor/boxes/$box" >"$vagrantboxdir/$vendor-VAGRANTSLASH-$box/metadata_url"
