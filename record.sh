#!/bin/sh

if ! command -v xwininfo >/dev/null 2>&1; then
    echo >&2 "xwininfo: command not found"
    exit 1
fi

date=$(date '+%Y-%m-%d_%H%M%S')
output="record_${date}.webm"

eval "$(xwininfo | sed -n \
               -e 's/.*Width:\s*\([0-9]\+\)/width=\1/p' \
               -e 's/.*Height:\s*\([0-9]\+\)/height=\1/p' \
               -e 's/.*Absolute upper-left X:\s*\([0-9]\+\)/x=\1/p' \
               -e 's/.*Absolute upper-left Y:\s*\([0-9]\+\)/y=\1/p')"

width=$(($width + 4))
height=$(($height + 4))
#x=$(($x + 1))
#y=$(($y + 1))

#echo $width
#echo $height
#echo $x
#echo $y

rm -f "$output"
# -c:v libx264rgb
ffmpeg -video_size ${width}x${height} -framerate 25 -f x11grab -i :0.0+${x},${y} -c:v libvpx -b:v 2500k -t 00:00:05 "$output"
