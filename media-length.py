#!/usr/bin/env python3

import sys
import re
import subprocess

if len(sys.argv) < 2:
    print("Usage: {:s} file ...".format(sys.argv[0]), file=sys.stderr)
    exit(1)

args = ['mediainfo', *sys.argv[1:]]
proc = subprocess.Popen(args, stdout=subprocess.PIPE)

time = re.compile(b'Duration\s+: ([0-9]*) min ([0-9]*) s')

durations = []
is_general = False
for line in proc.stdout:
    line = line.rstrip()
    if is_general:
        if line.startswith(b'Duration '):
            durations.append(line)
        if line == b'':
            is_general = False
    elif line == b'General':
        is_general = True

seconds = 0
for d in durations:
    m = time.match(d)
    min, sec = m.group(1), m.group(2)
    seconds = seconds + (int(min) * 60 + int(sec))

result = ''
tmp = seconds
while tmp >= 60:
    rem = tmp % 60
    result = '{:02d}'.format(rem) + (':' + result if result else result)
    tmp = tmp // 60
result = '{:02d}'.format(tmp) + (':' + result if result else result)
#print('{:d}:{:d}'.format(seconds // 60, seconds % 60))
print(result)
