#!/usr/bin/env runghc

module Main where

import System.Environment
import qualified Data.ByteString as B

main :: IO ()
main = do
  args <- getArgs
  if (args == [])
    then putStrLn . (<> " file") . ("Usage: " <>) =<< getProgName
    else cleanCR (head args)

cleanCR :: FilePath -> IO ()
cleanCR file = B.writeFile file . B.filter (/= 13) =<< B.readFile file
