#!/usr/bin/env python2

from __future__ import print_function
import sys
import urllib
import urllib2
import json
import base64
import hashlib
import datetime

USER  = ''
TOKEN = ''

class Request:
    def __init__(self):
        self.api = 'https://gitlab.com/api/v4'

    def request(self, sub_api, method=None, data=None):
        opener = urllib2.build_opener(urllib2.HTTPHandler)

        request = urllib2.Request(url=self.api + sub_api, headers={'Private-Token': TOKEN}, data=data)
        if method is not None:
            request.get_method = lambda: method

        ret = json.load(opener.open(request))

        return ret

class Tools:
    def lineToFingerprint(self, line):
        key = base64.b64decode(line.strip().split()[1].encode('ascii'))
        fp_plain = hashlib.md5(key).hexdigest()
        return ':'.join(a+b for a,b in zip(fp_plain[::2], fp_plain[1::2]))

class Command:
    def __init__(self):
        self._r = Request()
        self._t = Tools()

    def list(self, arg=None):
        j = self._r.request('/users/' + USER + '/projects')
        for i in j:
            print('%s (%s)' % (i['name'], i['visibility']))

    def branches(self, arg):
        name = arg[0]
        j = self._r.request('/projects/' + USER + '%2F' + name + '/repository/branches')
        for i in j:
            print('%s%s' % (i['name'], ' (protected)' if i['protected'] else ''))

    def create(self, arg):
        name = arg[0]
        visibility = None
        try:
            visibility = arg[1]
        except IndexError:
            pass
        data = {}
        data['name'] = name
        if visibility is not None:
            data['visibility'] = visibility
        try:
            self._r.request('/projects', data=urllib.urlencode(data))
            print(
"""Create a new repository
$ git clone https://gitlab.com/zanc/%s.git
$ cd %s
$ touch README.md
$ git add README.md
$ git commit -m "add README"
$ git push -u origin master

Existing Git repository
$ cd existing_repo
$ git remote rename origin old-origin
$ git remote add origin https://gitlab.com/zanc/%s.git
$ git push -u origin --all
$ git push -u origin --tags""" % (name, name, name))
        except urllib2.HTTPError:
            pass

    def visibility(self, arg):
        name = arg[0]
        visibility = arg[1]
        self._r.request('/projects/' + USER + '%2F' + name, method='PUT', data=urllib.urlencode({'visibility': visibility}))

    def remove(self, arg):
        input = raw_input(
"""Are you sure you want to remove the following repositories:
%s
[y/N]? """ % "\n".join(arg)
                 )

        if input.lower() == ('y'):
            for name in arg:
                self._r.request('/projects/' + USER + '%2F' + name, method='DELETE')

    def protect(self, arg):
        name = arg[0]
        branch = arg[1]
        self._r.request('/projects/' + USER + '%2F' + name + '/protected_branches/', method='POST', data=urllib.urlencode({'name': branch}))

    def unprotect(self, arg):
        name = arg[0]
        branch = arg[1]
        try: self._r.request('/projects/' + USER + '%2F' + name + '/protected_branches/' + branch, method='DELETE')
        except ValueError: pass

    def ssh(self, arg):
        cmd = arg[0]
        if cmd == 'list':
            j = self._r.request('/users/' + USER + '/keys')
            for i in j:
                key = self._t.lineToFingerprint(i['key'])
                created = datetime.datetime.strptime(i['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ').strftime('%Y-%m-%d')
                expires = 'never'
                if i['expires_at'] is not None:
                    expires = datetime.datetime.strptime(i['expires_at'], '%Y-%m-%dT%H:%M:%S.%fZ').strftime('%Y-%m-%d')
                print("""%s %s (id: %s)
created: %s   expires: %s
""" % (i['title'], key, i['id'], created, expires)) # i['id'], i['key']
        elif cmd == 'delete':
            id = arg[1]
            try: self._r.request('/user/keys/' + id, method='DELETE')
            except ValueError: pass
        elif cmd == 'add':
            key = arg[1]
            title = key[::-1][:key[::-1].find(' ')][::-1]
            self._r.request('/user/keys', method='POST', data=urllib.urlencode({'title': title, 'key': key}))

class Main():
    def __init__(self):
        self.c = Command()

        self.name = sys.argv[0].split('/')[-1]

        try:
            self.cmd = sys.argv[1]
            self.arg = sys.argv[2:]
        except IndexError:
            self.print_help()

    def main(self):
        if not USER or not TOKEN:
            print("USER or TOKEN variable is empty", file=sys.stderr)
            sys.exit(1)

        if hasattr(self.c, self.cmd):
            getattr(self.c, self.cmd)(self.arg)
        else:
            self.print_help()

    def print_help(self):
        #cmds = "|".join([x for x in dir(self.c) if not x.startswith('__') and not x.endswith('__') and not x.startswith('_')])
        #print("usage: %s %s" % (self.name, cmds), file=sys.stderr)
        print("""Usage:
 %s <command>

Commands:
 branches <repo>
 create <repo> <public|private|internal>
 list
 protect <repo> <branch>
 remove <repo> [<repo> ...]
 ssh add <key>
 ssh delete <id>
 ssh list
 unprotect <repo> <branch>
 visibility <repo> <public|private|internal>""" % self.name, file=sys.stderr)
        sys.exit(1)

if __name__ == '__main__':
    try:
        main = Main()
        main.main()
    except KeyboardInterrupt:
        sys.exit(1)
