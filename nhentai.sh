#!/bin/sh

if [ $# -lt 1 ]; then
  echo "Usage: ${0##*/} url" >&2
  exit 1
fi

url="$1"
shift

if [ "${url#https://nhentai.net/g/}" = "$url" ]; then
  if echo "$url" | grep -q -e '^[0-9]*$'; then
    url="https://nhentai.net/g/$url"
  else
    echo "${0##*/}: $url: invalid url" >&2
    exit 1
  fi
fi

imgs=$(curl -sL "$url" | grep -o -e 'img src="https://t\.nhentai\.net/[^"]*' | sed -e 's/.*"//' -e 's/t\.nhentai/i\.nhentai/' -e 's,\([^/]*\)t\(\..*\)$,\1\2,' | grep -v -e cover -e thumb)

maxlen=$(printf '%s' "$(echo "$imgs" | tail -1 | sed -e 's,.*/,,' -e 's/\..*//')" | wc -c)

for i in $imgs; do
  fname="${i##*/}"
  while [ $(printf '%s' "${fname%.*}" | wc -c) -lt $maxlen ]; do
    fname="0$fname"
  done
  if [ -f "$fname" ]; then
    continue
  fi
  echo "==> Fetching $i"
  while :; do
    curl -L -R -g -C - -o "$fname".part "$i"
    retval="$?"
    case "$retval" in
      0) mv -f "$fname".part "$fname" ;;
      33) rm -f "$fname".part; continue ;;
      18|28|56) continue ;;
      *) exit "$retval" ;;
    esac
    break
  done
done
