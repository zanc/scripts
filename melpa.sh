#!/bin/sh

command="$1"
if [ -z "$command" ]; then
    echo >&2 "Usage: $0 fetch|refetch|search|show"
    exit 1
fi
shift

if ! command -v jq >/dev/null 2>&1; then
    echo >&2 "jq: command not found"
    exit 1
fi

check() {
    if [ ! -e "$HOME/.cache/melpa/recipes.json" ]; then
        echo >&2 "No recipes.json found in $HOME/.cache/melpa."
        echo >&2 "Try running \`$0 fetch' to fetch it."
        exit 1
    fi
}

fetch() {
    local retval
    if [ ! -e "$HOME/.cache/melpa/recipes.json" ]; then
        mkdir -p "$HOME/.cache/melpa"
        echo >&2 ">> https://melpa.org/recipes.json"
        while :; do
            curl -LRC- -o "$HOME/.cache/melpa/recipes.json".part https://melpa.org/recipes.json
            retval="$?"
            case "$retval" in
                0) mv -f "$HOME/.cache/melpa/recipes.json".part "$HOME/.cache/melpa/recipes.json" ;;
                33) rm -f "$HOME/.cache/melpa/recipes.json".part; continue ;;
                18|28|56|35) continue ;;
                *) exit "$retval" ;;
            esac
            break
        done
    fi
}

case "$command" in
    fetch)
        fetch
        ;;
    refetch)
        rm -f "$HOME/.cache/melpa/recipes.json"
        fetch
        ;;
    search)
        package="$1"
        if [ -z "$package" ]; then
            echo >&2 "Usage: $0 $command package"
            exit 1
        fi
        shift

        check

        jq -r ". | keys | .[] | select(test(\"$package\"))" "$HOME/.cache/melpa/recipes.json"
        ;;
    show)
        package="$1"
        if [ -z "$package" ]; then
            echo >&2 "Usage: $0 $command package"
            exit 1
        fi
        shift

        check

        jq -r ".\"$package\"" "$HOME/.cache/melpa/recipes.json"
        ;;
esac
