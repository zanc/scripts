#!/usr/bin/env python3

import sys, zlib, re
from io import DEFAULT_BUFFER_SIZE

if len(sys.argv) <= 1:
    print("Usage: {:s} file ...".format(sys.argv[0]), file=sys.stderr)
    sys.exit(1)

for file in sys.argv[1:]:
    m = re.search(r'[0-9a-fA-F]{8}', file)
    file_crc = None
    if m:
        file_crc = eval('0x' + m.group().upper())
    else:
        print("{:s}: no crc32 found in the filename".format(file))
        continue

    crc = 0
    with open(file, 'rb') as f:
        buf = f.read(DEFAULT_BUFFER_SIZE)
        while buf != b'':
            crc = zlib.crc32(buf, crc)
            buf = f.read(DEFAULT_BUFFER_SIZE)

    buf_crc = crc

    matched = "FAILED"
    if file_crc == buf_crc:
        matched = "OK"

    print("{:s}: {:s}".format(file, matched))
