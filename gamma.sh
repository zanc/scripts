#!/usr/bin/env bash
# This script implements an improved version of the gamma trick used to make
# thumbnail images on reddit/4chan look different from the full-size image.
#
# Some backstory, explanation and example (slightly NSFW):
#  https://www.reddit.com/r/touhou/comments/6e6lga/a/di83t02/
#
# This trick works on at least Reddit, 4chan and similar imageboards, Google
# Drive, Slack, and probably many others. It does not work on Twitter, as
# Twitter always preprocesses PNG images and strips the gAMA chunk. It does,
# however, work with e.g. imgur embed previews on Twitter.
#
# *Different* one-liner trick that works on Twitter (for grayscale images):
#  https://twitter.com/marcan42/status/869858577116086272

high="$1" # High image (full-size original view)
low="$2" # Low image (thumbnail) (should be the same size)

output="output.png"
[ ! -z "$3" ] && output="$3" # Output image

size=$(convert "$high" -format "%wx%h" info:)

# Give a slight brightness boost to the high source, then apply the gamma.
# This ensures that the pixels look mostly white.
convert "$high" -alpha off +level 3.5%,100% -gamma 20 high_gamma.png

low_gamma="-alpha off -gamma 0.8 +level 0%,77%"

# To get rid of the slight "halo" of the high image, we're going to cancel it out
convert \( "$low" $low_gamma \) high_gamma.png \
           -compose Mathematics -define compose:args='0,-0.33,1,0.33' \
           -composite low_adjusted.png

# Now compose both images together using the mask, then set the gamma metadata.
convert low_adjusted.png high_gamma.png -size $size pattern:gray25 -composite \
        -set gamma 0.022727 -define png:include-chunk=none,gAMA "$output"

rm high_gamma.png low_adjusted.png
