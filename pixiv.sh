#!/bin/sh
# vim: se ts=2 sw=2 et:
#
# Reference:
# https://qiita.com/l7u7ch/items/af6373042d4421bf002b
# https://github.com/upbit/pixivpy/blob/master/pixivpy3/api.py

# Set the following
username=""
password=""

if [ $# -eq 0 ]; then
  echo "Usage: $0 id" >&2
  exit 1
fi

id="$1"; shift

images=$(curl -s -L -g "https://www.pixiv.net/member_illust.php?mode=manga&illust_id=$id" | grep -o -e 'pixiv\.context\.images\[[^]]*\] = "[^"]*"' | sed -e 's/[^"]*"//' -e 's/"$//' -e 's,\\/,/,g' -e 's,/img-master/,/img-original/,' -e 's/_master[^.]*\(\..*\)$/\1/')

#curl -X POST \
#  -d 'client_id=MOBrBDS8blbauoSck0ZfDbtuzpyT' \
#  -d 'client_secret=lsACyCD94FhDUtGTXi3QzcFE2uU1hqtDaKeqrdwj' \
#  -d 'grant_type=password' \
#  -d "username=$username" \
#  -d "password=$password" \
#  https://oauth.secure.pixiv.net/auth/token

# curl -X GET \
#  -H 'Authorization:Bearer [ACCESS_TOKEN]' \
#  'https://public-api.secure.pixiv.net/v1/ranking/all?mode=daily&image_sizes=large'

for i in $images; do
  fname="${i##*/}"
  if [ -f "$fname" ]; then
    continue
  fi
  while :; do
    echo "==> Fetching $i"
    curl -X GET -H 'Referer: https://app-api.pixiv.net/' -L -R -C - -Y 1 -y 10 -o "$fname".part "$i"
    ret="$?"
    case "$ret" in
      0) mv -f "$fname".part "$fname" ;;
      33) rm -f "$fname".part; continue ;;
      18|28|56|35) continue ;;
      *) exit "$ret" ;;
    esac
    break
  done
done
