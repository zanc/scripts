#!/bin/sh
# vim: se ts=2 sw=2 et:

#vendor=laravel
#box=homestead
#version=6.3.0
#provider=virtualbox

if [ $# -lt 3 ]; then
  echo "usage: ${0##*/} box version provider" >&2
  exit 1
fi

vendor=${1%/*}
box=${1#*/}
shift
version=$1; shift
provider=$1; shift

boxname=$vendor-VAGRANTSLASH-$box-$version-$provider.box

if [ -f "$boxname" ]; then
  echo "==> $boxname is already downloaded"
  exit 0
fi
while :; do
  echo "==> Downloading $boxname"
  curl -L -R -g -k -C - -o "$boxname".part https://app.vagrantup.com/$vendor/boxes/$box/versions/$version/providers/$provider.box
  retval="$?"
  case "$retval" in
    0) mv -f "$boxname".part "$boxname" ;;
    33) rm -f "$boxname".part; continue ;;
    18|28|56) continue ;;
    *) exit $retval ;;
  esac
  break
done
