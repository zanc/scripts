#!/usr/bin/env bash

curl="curl -b SSID=AuHBgaFEtIzq8_TQc \
           -b HSID=AZqcGICHqG80Qc03P \
           -b SID=RggYbR2jtdXPJYIG9fqzmuoZjVwrGSLHbNShJGnRLhDysoszjeILWqMLvL1r4_-JxfQ_6Q. \
           -b OSID=RggYbQSJIWC8i5yWnW3WgIOHDl5-208BK7pT-bVzyIM1ffsJgOZGGN47CskQJ5IHgJ1KvQ."

page=$($curl -sLg 'https://mail.google.com/?ui=html')

if echo "$page" \
        | tr '\n' ' ' \
        | grep -Po '<table.*?</table>' \
        | sed -ne 5p \
        | grep -Po '<td.*?</td>' \
        | sed -ne 2p \
        | grep -q '<b>'; then
    echo "new mail"
fi
