urls=(
    'https://video.twimg.com/ext_tw_video/1580288674201231361/pu/pl/MmEOIXOdIJ_2e0O5.m3u8?variant_version=1&tag=12&container=fmp4'
    'https://video.twimg.com/ext_tw_video/1580288462065963008/pu/pl/GulTMJHG1IaYTnD_.m3u8?variant_version=1&tag=12&container=fmp4'
    'https://video.twimg.com/ext_tw_video/1578800299439013894/pu/pl/HwUmqY1jDWT0TnDR.m3u8?variant_version=1&tag=12&container=fmp4'
    'https://video.twimg.com/ext_tw_video/1579099461355274242/pu/pl/HePBVJqEG1FTbCQR.m3u8?variant_version=1&tag=12&container=fmp4'
    'https://video.twimg.com/ext_tw_video/1579099278982651905/pu/pl/P3jRbZIggdlB0-em.m3u8?variant_version=1&tag=12&container=fmp4'
)
clean_dups="awk {if(!dups[\$0]++){print}}"

for url in "${urls[@]}"; do
    index="${url##*/}"
    index="${index%%\?*}"
    #echo ">> $index"
    if [[ ! -e ./"$index" ]]; then
        while ! curl -LRgC- -o ./"$index".part "$url"; do
            :
        done
        mv ./"$index".part ./"$index"
    fi

    url="https://video.twimg.com$(grep '/720x[0-9]\+/' "$index")"
    index="${url##*/}"
    index="${index%%\?*}"
    echo ">> $index"
    if [[ ! -e ./"$index" ]]; then
        while ! curl -LRgC- -o ./"$index".part "$url"; do
            :
        done
        mv ./"$index".part ./"$index"
    fi
    segments=$(sed -n \
                   -e 's/#EXT-X-MAP:URI="\(.*\)"/\1/p' \
                   -e '/^\/ext_tw_video\//p' \
                   ./"$index" | $clean_dups)
    for j in $segments; do
        url="https://video.twimg.com$j"
        fname="${j##*/}"
        if [[ -e ./"$fname" ]]; then
            continue
        fi
        while ! curl -LRgC- -o ./"$fname".part "$url"; do
            :
        done
        mv ./"$fname".part ./"$fname"
    done
    index_new="${index%.m3u8}-new.m3u8"
    cp ./"$index" ./"$index_new"
    sed -i -e 's,\(#EXT-X-MAP:URI="\).*/\(.*\)\("\),\1\2\3,' -e 's,^/ext_tw_video/.*/\(.*\),\1,' ./"$index_new"
done
